ip addr | grep master | awk -F'master ' '{print $2}' | cut -d' ' -f1 | sort | uniq > /tmp/allbond
> /tmp/result
while read bond
do
        ip addr | grep $bond | grep -v inet | cut -d':' -f2 | cut -d'@' -f1 > /tmp/interface
        while read interface
        do
                echo $interface
        done < /tmp/interface
        printf "[i]nternal , [p]ublic or [t]rusted ? (press i,p or t):"
        read zone </dev/tty
        case "$zone" in
        i) while read interface
        do
                echo "firewall-cmd --permanent --change-interface="$interface" --zone=internal" >> /tmp/result
        done < /tmp/interface
           ;;
        p) while read interface
        do
                echo "firewall-cmd --permanent --change-interface="$interface" --zone=public" >> /tmp/result
        done < /tmp/interface
           ;;
        t) while read interface
        do
                echo "firewall-cmd --permanent --change-interface="$interface" --zone=trusted" >> /tmp/result
        done < /tmp/interface
           ;;
        esac
        echo "" >> /tmp/result
done < /tmp/allbond
ip addr | grep -v "master\|MASTER\|lo" | grep qlen | cut -d':' -f2 | cut -d'@' -f1> /tmp/interface
while read interface
do
    echo $interface
done < /tmp/interface
        printf "[i]nternal or [p]ublic ? (press i,p or t):"
        read zone </dev/tty
        case "$zone" in
        i) while read interface
        do
                echo "firewall-cmd --permanent --change-interface="$interface" --zone=internal" >> /tmp/result
        done < /tmp/interface
           ;;
        p) while read interface
        do
                echo "firewall-cmd --permanent --change-interface="$interface" --zone=public" >> /tmp/result
        done < /tmp/interface
           ;;
        t) while read interface
        do
                echo "firewall-cmd --permanent --change-interface="$interface" --zone=trusted" >> /tmp/result
        done < /tmp/interface
           ;;
        esac
        echo "" >> /tmp/result

echo "---------------------------------------------------------------------------------------"
echo
echo "run this command"
echo
cat /tmp/result
echo "firewall-cmd --reload"
echo "save command to /tmp/result file"
scp /tmp/result .
chmod +x result
#rm -f /tmp/result