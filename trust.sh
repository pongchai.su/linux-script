#!/usr/bin/expect

set timeout 20
set ip [lindex $argv 0]
spawn ssh-copy-id "$ip"
expect {
        "(yes/no)?" {
                send "yes\r"
                exp_continue
        }
        "password:" {
                send "password\r";
                exp_continue
        }
}