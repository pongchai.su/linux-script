if [ ! -f ~/.ssh/id_rsa.pub ]; then
        ssh-keygen -t rsa
fi
ssh-copy-id cdnedge@10.18.12.42

if [ ! -f WowzaStreamingEngine-4.8.0-linux-x64-installer.run ]; then
        scp cdnedge@10.18.12.42:/home/cdnedge/WowzaStreamingEngine-4.8.0-linux-x64-installer.run .
        chmod +x WowzaStreamingEngine-4.8.0-linux-x64-installer.run
fi

if [ ! -f jdk-11.0.7_linux-x64_bin.rpm ]; then
        scp cdnedge@10.18.12.42:/home/cdnedge/jdk-11.0.7_linux-x64_bin.rpm .
fi

ssh cdnedge@10.18.12.42 "tar czvf /tmp/smil.tar.gz /usr/local/WowzaStreamingEngine/content/smil/"
scp cdnedge@10.18.12.42:/tmp/smil.tar.gz .

oldJre=$(rpm -qa | grep jre)
if [[ ! -z "${oldJre// }" ]]; then
        rpm -e $oldJre
fi
rpm -ivh jdk-11.0.7_linux-x64_bin.rpm

/usr/local/WowzaStreamingEngine/uninstall
./WowzaStreamingEngine-4.8.0-linux-x64-installer.run

tar xzvf smil.tar.gz -C /
rsync -avz cdnedge@10.18.12.42:/usr/local/WowzaStreamingEngine/ /usr/local/WowzaStreamingEngine/
chown -R cdnedge:cdnedge /usr/local/WowzaStreamingEngine/

rm -f /usr/local/WowzaStreamingEngine/java
ln -s /usr/java/latest /usr/local/WowzaStreamingEngine/java

export http_proxy=10.18.12.151:80
export https_proxy=10.18.12.151:80
yum install net-tools -y

mgmtinterface=$(ls /sys/class/net | grep "mgmt")
mgmtip=$(ifconfig $mgmtinterface | grep 'netmask' | awk -F' ' '{print $2}')
sed -i "s/xxxxxxxx/$mgmtip/g" /usr/local/WowzaStreamingEngine/conf/Server.xml

systemctl restart WowzaStreamingEngine
systemctl restart WowzaStreamingEngineManager